# Python Challenge

## General description
Python challenge for Qualifinds

## Project setup
Clone the repo or download .zip file:
```
$ git clone git@bitbucket.org:gaval_997/qualifinds_ha.git
```

Run the following commands:
```
$ cd qualifinds_ha
$ pipenv shell
$ pip install -r requirements.txt
```

There should already be a superuser created, if not, create a new one (in case you want to use django-admin):
```
Username: hector
Password: 12345
```

The SQLite Database already exists, but in case there were issues with it, run the following commands:
```
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser
```

## Tests
Run project tests with the following command:
```
$ python manage.py test
```

## Extra info
I included a `Postman` collection and environment in the project folder. Please use them to test the endpoints.

## Author
- Hector Gerardo Avalos Crisostomo
- hg.avalosc97@gmail.com
- +52 33 39 70 94 48
