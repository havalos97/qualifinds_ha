from django.db import models


class User(models.Model):
    class Meta:
        verbose_name_plural = 'users'

    email = models.CharField(max_length=128, unique=True, null=False, blank=False)
    password = models.CharField(max_length=128, null=False, blank=False)
    full_name = models.CharField(max_length=256, null=False, blank=False)

    def __str__(self):
        return self.full_name
