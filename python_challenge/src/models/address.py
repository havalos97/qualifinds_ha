from django.db import models
from .user import User


class Address(models.Model):
    class Meta:
        verbose_name_plural = 'addresses'

    postal_code = models.CharField(max_length=64)
    municipality = models.CharField(max_length=64)
    state = models.CharField(max_length=64)
    is_primary_address = models.BooleanField(default=False)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=False,
        blank=False
    )

    def __str__(self):
        return f'{self.postal_code}, {self.municipality}. {self.state}'
