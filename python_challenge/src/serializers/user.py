from rest_framework import serializers
from python_challenge.src.models import User


class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class UserCreateSerializer(serializers.Serializer):
    """
    Validate that menu´s date was greater or equal than today.
    """

    email = serializers.CharField(
        min_length=1,
        max_length=128
    )
    full_name = serializers.CharField(
        min_length=1,
        max_length=128
    )
    password = serializers.CharField(
        min_length=1,
        max_length=128
    )

    def create(self, address_data):
        return User.objects.create(**address_data)
