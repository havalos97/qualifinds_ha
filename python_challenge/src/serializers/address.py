from rest_framework import serializers
from python_challenge.src.models import Address, User
from python_challenge.src.serializers import UserModelSerializer


class AddressModelSerializer(serializers.ModelSerializer):
    user = UserModelSerializer()

    class Meta:
        model = Address
        fields = '__all__'


class AddressCreateSerializer(serializers.Serializer):
    """
    Validate that menu´s date was greater or equal than today.
    """

    state = serializers.CharField(
        min_length=1,
        max_length=64
    )
    municipality = serializers.CharField(
        min_length=1,
        max_length=64
    )
    postal_code = serializers.CharField(
        min_length=1,
        max_length=64
    )
    is_primary_address = serializers.BooleanField(
        default=False
    )
    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all()
    )

    def create(self, address_data):
        return Address.objects.create(**address_data)

    def validate(self, data):
        if not data['user']:
            raise serializers.ValidationError(
                'Address requires a user.'
            )
        return data
