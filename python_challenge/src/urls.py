from django.urls import include, path
from rest_framework import routers
from python_challenge.src.views import *

router = routers.DefaultRouter()
router.register(r'users', UserViewSet, basename='user')

urlpatterns = [
    path('', include(router.urls)),
    path('users/<int:user_id>/addresses/', AddressViewSet.as_view({'post': 'create'})),
    path('users/<int:user_id>/update-password/', AddressViewSet.as_view({'post': 'update_password'})),
    path('users/<int:user_id>/addresses/<int:address_id>/', AddressViewSet.as_view({'post': 'set_primary_address'})),
]
