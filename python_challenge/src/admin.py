from django.contrib import admin
from python_challenge.src.models import *

# Register your models here.
admin.site.register(User)
admin.site.register(Address)
