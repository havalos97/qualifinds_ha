from rest_framework import viewsets, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from python_challenge.src.serializers import AddressCreateSerializer, AddressModelSerializer
from python_challenge.src.models import Address, User


class AddressViewSet(viewsets.ViewSet):
    """
    ViewSet for Address model
    """

    def set_primary_address(self, request, *args, **kwargs):
        user_id = kwargs['user_id']
        primary_address_id = kwargs['address_id']

        try:
            User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return Response({
                "error": "User id not found!"
            }, status=status.HTTP_404_NOT_FOUND)

        try:
            Address.objects.get(pk=primary_address_id)
        except Address.DoesNotExist:
            return Response({
                "error": "Address id not found!"
            }, status=status.HTTP_404_NOT_FOUND)

        user_addresses = Address.objects.filter(user_id=user_id)
        address_ids = []
        for user_address in user_addresses:
            address_data = AddressModelSerializer(user_address).data
            address_ids.append(address_data['id'])

        for user_address_id in address_ids:
            try:
                address_to_update = Address.objects.get(pk=user_address_id)
            except Address.DoesNotExist:
                continue

            address_to_update.is_primary_address = False
            if primary_address_id == user_address_id:
                address_to_update.is_primary_address = True
            address_to_update.save()

        return Response({
            "status": "Primary address was set successfully!"
        }, status=status.HTTP_200_OK)

    def update_password(self, request, *args, **kwargs):
        user_id = kwargs['user_id']

        try:
            user_to_update = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return Response({
                "error": "User id not found!"
            }, status=status.HTTP_404_NOT_FOUND)

        user_to_update.password = request.data['password']
        user_to_update.save()

        return Response({
            "status": "Password was changed successfully!"
        }, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        """
        Create an Address
        """
        user_id = kwargs['user_id']

        # Make sure user_id exists
        try:
            User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return Response({
                "error": "User id not found!"
            }, status=status.HTTP_404_NOT_FOUND)

        # Get number of existing addresses for that user_id
        user_addresses = Address.objects.select_related().filter(user_id=user_id)
        if len(user_addresses) < 3:
            # Build new address dict
            new_address_data = {k: v for k, v in request.data.items()}
            new_address_data['user'] = user_id

            # Create object using serializer
            try:
                serializer = AddressCreateSerializer(data=new_address_data)
                serializer.is_valid(raise_exception=True)
                address = serializer.save()
            except ValidationError:
                return Response({
                    "error": f'Could not create address for user {user_id}'
                }, status=status.HTTP_400_BAD_REQUEST)

            data = AddressModelSerializer(address).data
            return Response(data, status=status.HTTP_201_CREATED)
        return Response({
            "error": "This user has already three addresses related to it."
        }, status=status.HTTP_304_NOT_MODIFIED)
