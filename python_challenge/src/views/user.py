from rest_framework import viewsets, status
from rest_framework.exceptions import ValidationError
from rest_framework.pagination import PageNumberPagination
from django.db import IntegrityError
from rest_framework.response import Response
from python_challenge.src.models import User
from python_challenge.src.serializers import UserModelSerializer, UserCreateSerializer, AddressModelSerializer


class UserViewSet(viewsets.ViewSet):
    """
    ViewSet for User model
    """

    def list(self, request):
        queryset = User.objects.all()
        paginator = PageNumberPagination()
        page = paginator.paginate_queryset(queryset, request)

        if page is not None:
            serializer = UserModelSerializer(page, many=True)
            return paginator.get_paginated_response(serializer.data)

        serializer = UserModelSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None):
        try:
            user = User.objects.get(pk=pk)
        except User.DoesNotExist:
            return Response({
                "error": "User id not found!"
            }, status=status.HTTP_404_NOT_FOUND)
        serializer = UserModelSerializer(user)

        found_addresses = user.address_set.all()
        user_addresses = []
        primary_address = None
        for found_address in found_addresses:
            address_data = AddressModelSerializer(found_address).data
            new_address_data = {
                "id": address_data['id'],
                "postal_code": address_data['postal_code'],
                "municipality": address_data['municipality'],
                "state": address_data['state'],
                "is_primary_address": address_data['is_primary_address']
            }

            user_addresses.append(new_address_data)

            if address_data['is_primary_address']:
                primary_address = new_address_data

        return Response({
            **serializer.data,
            "primary_address": primary_address,
            "addresses": user_addresses
        }, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        """
        Create an Address
        """
        try:
            serializer = UserCreateSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            user = serializer.save()
        except ValidationError:
            return Response({
                "error": f'Could not create user.'
            }, status=status.HTTP_400_BAD_REQUEST)
        except IntegrityError:
            return Response({
                "error": f'The provided email already exists, please use a different one.'
            }, status=status.HTTP_400_BAD_REQUEST)

        data = UserModelSerializer(user).data
        return Response(data, status=status.HTTP_201_CREATED)
