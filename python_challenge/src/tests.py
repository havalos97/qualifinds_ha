from django.test import TestCase
from django.db import IntegrityError
from rest_framework.test import APIRequestFactory
from rest_framework import status
from python_challenge.src.models import User
from python_challenge.src.views import AddressViewSet, UserViewSet


class UserTestCase(TestCase):
    def setUp(self):
        User.objects.create(
            full_name='Hector Avalos',
            email='hg.avalosc97@gmail.com',
            password='12345'
        )

    def test_users_are_defined(self):
        hector = User.objects.get(email='hg.avalosc97@gmail.com')
        self.assertEqual(hector.full_name, "Hector Avalos")

    def test_database_integrity(self):
        with self.assertRaises(IntegrityError):
            # Make sure email is unique, test for IntegrityError thrown
            User.objects.create(
                full_name='John Doe',
                email='hg.avalosc97@gmail.com',
                password='54321'
            )

    def test_get_user_detail(self):
        user = User.objects.get(email='hg.avalosc97@gmail.com')
        request = APIRequestFactory().get(f'/api/users/{user.pk}/')
        view = UserViewSet.as_view({'get': 'retrieve'})
        response = view(request, pk="1")
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'addresses')

    def test_create_user_address(self):
        user = User.objects.get(email='hg.avalosc97@gmail.com')
        addresses = [
            {
                "state": "Jalisco",
                "municipality": "Zapopan",
                "postal_code": "45123"
            },
            {
                "state": "Nayarit",
                "municipality": "Tepic",
                "postal_code": "45321"
            },
            {
                "state": "Sonora",
                "municipality": "Caborca",
                "postal_code": "45789"
            },
            {
                "state": "Nuevo Leon",
                "municipality": "Cumbres",
                "postal_code": "45987"
            }
        ]

        for idx, address in enumerate(addresses):
            request = APIRequestFactory().post(f'/api/users/{user.pk}/addresses/', address, format='json')
            view = AddressViewSet.as_view({'post': 'create'})
            response = view(request, user_id="1")

            if idx == 3:
                # Fourth address must not be created, 304 status expected.
                self.assertEquals(response.status_code, status.HTTP_304_NOT_MODIFIED)
            else:
                self.assertEquals(response.status_code, status.HTTP_201_CREATED)
